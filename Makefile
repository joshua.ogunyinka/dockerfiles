THIS_FILE := $(lastword $(MAKEFILE_LIST))

all: image

prune:
	docker image prune

image: docgen
	docker build --tag docgen:devel .

build-tiab-image:
	docker build --file tiab-debian.dockerfile .
